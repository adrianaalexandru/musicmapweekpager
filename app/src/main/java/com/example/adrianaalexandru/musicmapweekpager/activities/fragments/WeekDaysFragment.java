package com.example.adrianaalexandru.musicmapweekpager.activities.fragments;

import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import com.example.adrianaalexandru.musicmapweekpager.R;
import com.example.adrianaalexandru.musicmapweekpager.adapters.WeekPagerAdapter;
import com.example.adrianaalexandru.musicmapweekpager.utils.ChangeViewWeightAnimation;
import com.example.adrianaalexandru.musicmapweekpager.utils.DateUtils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.Date;

/**
 * Created by Adriana on 04/12/2016.
 */
public class WeekDaysFragment extends Fragment {
    private static final String TAG = "WeekDaysFragment";
    /**
     * Flag for week number field
     */
    private static final String WEEK_NO = "WEEK_NO";
    private static final String LISTENER = "LISTENER";
    /**
     * The duration for the animations played at new day selection
     */
    private static final int CHANGE_SELECTED_DAY_ANIM_DURATION = 500;
    /**
     * Current shown week number
     */
    private int weekNo;

    /**
     * The index of the current selected day
     */
    private int currentSelectedDayIndex;
    /**
     * The first day of the current displayed week
     */
    private DateTime firstDayOfTheCurrentWeek;
    /**
     * The 7 text views associated with the week days
     */
    private TextView[] daysTextViews;
    /**
     * The view listener
     */
    private WeekPagerAdapter.ViewListener viewListener;


    public static WeekDaysFragment newInstance(int weekNo, WeekPagerAdapter.ViewListener viewListener) {
        WeekDaysFragment fragmentFirst = new WeekDaysFragment();
        Bundle args = new Bundle();
        args.putInt(WEEK_NO, weekNo);
        args.putSerializable(LISTENER, viewListener);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weekNo = getArguments().getInt(WEEK_NO, 0);
        viewListener = (WeekPagerAdapter.ViewListener) getArguments().getSerializable(LISTENER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_week, container, false);

        firstDayOfTheCurrentWeek = DateUtils.getWeekFromWeekNo(weekNo);
        currentSelectedDayIndex = getDayToBeSelectedIndex(firstDayOfTheCurrentWeek);

        configureDaysViews(view, firstDayOfTheCurrentWeek);
        changeToDefaultWeekSelection();
        return view;
    }

    /**
     * Change UI & fields values to the default state : selected day is either first day of the week
     * or the current day (if the displayed week corresponds to the current one)
     */
    public void changeToDefaultWeekSelection() {
        int dayToBeSelectedIndex = getDayToBeSelectedIndex(firstDayOfTheCurrentWeek);
        onDayTextViewSelected(dayToBeSelectedIndex, false);
    }

    public Date getSelectedDate() {
        return firstDayOfTheCurrentWeek.plusDays(currentSelectedDayIndex).toDate();
    }


    public void findViewsByIds(View view) {
        daysTextViews = new TextView[7];
        daysTextViews[0] = (TextView) view.findViewById(R.id.layout_week_day1_textview);
        daysTextViews[1] = (TextView) view.findViewById(R.id.layout_week_day2_textview);
        daysTextViews[2] = (TextView) view.findViewById(R.id.layout_week_day3_textview);
        daysTextViews[3] = (TextView) view.findViewById(R.id.layout_week_day4_textview);
        daysTextViews[4] = (TextView) view.findViewById(R.id.layout_week_day5_textview);
        daysTextViews[5] = (TextView) view.findViewById(R.id.layout_week_day6_textview);
        daysTextViews[6] = (TextView) view.findViewById(R.id.layout_week_day7_textview);
    }


    /**
     * Get the day which will initially be selected in the displayed week
     * Selected day will be either first day of the week
     * or the current day (if the displayed week corresponds to the current one)
     *
     * @param currentWeekShown first day of the displayed week
     * @return the week day index to be marked as selected
     */
    public int getDayToBeSelectedIndex(DateTime currentWeekShown) {
        int dayToBeSelectedIndex = 0;
        if (DateUtils.isDayInWeek(new Date(), currentWeekShown)) {
            DateTime today = new DateTime();
            dayToBeSelectedIndex = Days.daysBetween(currentWeekShown, today).getDays();
        }
        return dayToBeSelectedIndex;
    }

    /**
     * Initialize and customize properly the days text views
     * Change days views' content, background, tags, listeners
     *
     * @param layout           The parent layout
     * @param currentWeekShown The first day of the displayed week
     */
    public void configureDaysViews(View layout, DateTime currentWeekShown) {
        findViewsByIds(layout);
        DateTime currentSelectedDay = currentWeekShown.plusDays(currentSelectedDayIndex);
        int currentMonth = currentSelectedDay.getMonthOfYear();
        for (int i = 0; i < 7; i++) {
            final int dayIndex = i;
            Log.v(TAG, "final var " + dayIndex);
            daysTextViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onDayTextViewSelected(dayIndex, true);
                }
            });
            if (currentWeekShown.getMonthOfYear() != currentMonth) {
                updateUIDayTextView(daysTextViews[i],
                        ContextCompat.getColor(getContext(), R.color.lightgray),
                        ContextCompat.getDrawable(getContext(), R.drawable.lightgray_circle));
            }
            daysTextViews[i].setText(currentWeekShown.getDayOfMonth() + "");
            currentWeekShown = currentWeekShown.plusDays(1);
        }
    }

    public void onDiffMonthChosen(int newSelectedDayIndex) {
        DateTime newSelectedDay = firstDayOfTheCurrentWeek.plusDays(newSelectedDayIndex);
        for (int i = 0; i < 7; i++) {
            if (newSelectedDay.getDayOfMonth() !=
                    (firstDayOfTheCurrentWeek.plusDays(i).getDayOfMonth())) {
                if ((firstDayOfTheCurrentWeek.plusDays(i).getMonthOfYear()) !=
                        newSelectedDay.getMonthOfYear()) {
                    updateUIDayTextView(daysTextViews[i],
                            ContextCompat.getColor(getContext(), R.color.lightgray),
                            ContextCompat.getDrawable(getContext(), R.drawable.lightgray_circle));
                } else {
                    updateUIDayTextView(daysTextViews[i],
                            ContextCompat.getColor(getContext(), R.color.gray),
                            ContextCompat.getDrawable(getContext(), R.drawable.gray_circle));
                }
            }
        }
    }


    public void updateUIDayTextView(TextView textView, int textColor, Drawable
            backgroudDrawable) {
        textView.setTextColor(textColor);
        textView.setBackground(backgroudDrawable);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            if (!isVisibleToUser) {
                if (daysTextViews != null) {
                    changeToDefaultWeekSelection();
                }
            } else {
                onFragmentVisible();
            }
        }
    }

    /**
     * Changes that will be made when a new day is selected
     * Make UI updates on the previous selected day & new selected day
     *
     * @param dayIndex newly selected day index
     */
    public void onDayTextViewSelected(int dayIndex, boolean notifyListener) {
        DateTime newSelectedDay = firstDayOfTheCurrentWeek.plusDays(dayIndex);
        DateTime prevSelectedDay = firstDayOfTheCurrentWeek.plusDays(currentSelectedDayIndex);
        updateUIDayView(daysTextViews[currentSelectedDayIndex],
                getContext().getResources().getDimension(R.dimen.day_number_normal_size), 1.6f, 1,
                ContextCompat.getColor(getContext(), R.color.gray),
                ContextCompat.getDrawable(getContext(), R.drawable.gray_circle));

        updateUIDayView(daysTextViews[dayIndex],
                getContext().getResources().getDimension(R.dimen.day_number_big_size), 1, 1.6f,
                ContextCompat.getColor(getContext(), R.color.crimson_red),
                ContextCompat.getDrawable(getContext(), R.drawable.red_circle));
        if (newSelectedDay.getMonthOfYear() != prevSelectedDay.getMonthOfYear()) {
            onDiffMonthChosen(dayIndex);
        }
        currentSelectedDayIndex = dayIndex;
        if (viewListener != null && notifyListener) {
            viewListener.onNewDaySelected(newSelectedDay.toDate());
        }
    }

    /**
     * Update indicated day text view by modifying its text size, its weight, text color and background drawable
     *
     * @param textViewToBeUpdated The text view which will be modified
     * @param textDimenPx         The new dimen for the text size in pixels
     * @param startWeight         The start weight for the animation
     * @param endWeight           The end weight for the animation
     * @param textColor           The color for the text
     * @param backgroundDrawable  The drawable for the background
     */
    public void updateUIDayView(TextView textViewToBeUpdated, float textDimenPx,
                                float startWeight,
                                float endWeight, int textColor,
                                Drawable backgroundDrawable) {
        textViewToBeUpdated.setBackground(backgroundDrawable);
        Animation animation = new ChangeViewWeightAnimation(textViewToBeUpdated, startWeight, endWeight);
        animation.setDuration(CHANGE_SELECTED_DAY_ANIM_DURATION);
        startChangeTextSizeAnimation(textViewToBeUpdated, textDimenPx);
        textViewToBeUpdated.startAnimation(animation);
        textViewToBeUpdated.setTextColor(textColor);

    }

    /**
     * Animate text size change for a given text view with specified end text size
     *
     * @param textView the text view for which the change & the animation will be applied
     * @param endSize  the end text size
     */
    public void startChangeTextSizeAnimation(final TextView textView, float endSize) {
        ValueAnimator animator = ValueAnimator.ofFloat(textView.getTextSize(), endSize);
        animator.setDuration(CHANGE_SELECTED_DAY_ANIM_DURATION);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedValue = (float) valueAnimator.getAnimatedValue();
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, animatedValue);
            }
        });

        animator.start();
    }


    public void setViewListener(WeekPagerAdapter.ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public WeekPagerAdapter.ViewListener getViewListener() {
        return viewListener;
    }

    public void onFragmentVisible() {
        if (viewListener != null) {
            viewListener.onNewDaySelected(firstDayOfTheCurrentWeek.plusDays(currentSelectedDayIndex).toDate());
        }
    }
}
