package com.example.adrianaalexandru.musicmapweekpager.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

import com.example.adrianaalexandru.musicmapweekpager.R;
import com.example.adrianaalexandru.musicmapweekpager.activities.fragments.WeekDaysFragment;
import com.example.adrianaalexandru.musicmapweekpager.adapters.WeekPagerAdapter;
import com.example.adrianaalexandru.musicmapweekpager.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends ActionBarActivity {

    private static final String TAG = "MainActivity";
    private ViewPager weekViewPager;
    private WeekPagerAdapter weekPagerAdapter;
    private WeekPagerAdapter.ViewListener viewListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        weekViewPager = (ViewPager) findViewById(R.id.activity_main_week_view_pager);
        viewListener = new WeekPagerAdapter.ViewListener() {
            @Override
            public void onNewDaySelected(Date newSelectedDay) {
                MainActivity.this.getSupportActionBar().setTitle(new SimpleDateFormat("MMMM").format(newSelectedDay));
            }

        };
        weekPagerAdapter = new WeekPagerAdapter(getSupportFragmentManager());
        weekPagerAdapter.setViewListener(viewListener);
        weekViewPager.setAdapter(weekPagerAdapter);
        final int weekIndex = DateUtils.getWeeksSinceMinDate(new Date());
        weekViewPager.setCurrentItem(weekIndex);
        weekViewPager.post(new Runnable() {
            @Override
            public void run() {
                WeekDaysFragment fragment = ((WeekPagerAdapter) weekViewPager.getAdapter()).getFragment(weekIndex);
                if (fragment != null) {
                    fragment.onFragmentVisible();
                }
            }
        });
    }
}

