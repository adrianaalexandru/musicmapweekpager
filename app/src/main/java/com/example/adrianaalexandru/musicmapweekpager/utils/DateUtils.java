package com.example.adrianaalexandru.musicmapweekpager.utils;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Weeks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Adriana on 04/12/2016.
 */
public class DateUtils {
    private static final String TAG = "DateUtils";
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final String DEFAULT_MIN_DATE = "02/01/2010";
    private static final String DEFAULT_MAX_DATE = "01/01/2020";
    public static final SimpleDateFormat DEFAULT_SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT);

    public static DateTime minDate;
    public static DateTime maxDate;

    static {
        try {
            minDate = new DateTime(DEFAULT_SIMPLE_DATE_FORMAT.parse(DEFAULT_MIN_DATE));
            maxDate = new DateTime(DEFAULT_SIMPLE_DATE_FORMAT.parse(DEFAULT_MAX_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public static boolean isDayInWeek(Date day, DateTime firstDayOfTheWeek) {
        DateTime searchedDate = new DateTime(day);
        return searchedDate.isAfter(firstDayOfTheWeek) && searchedDate.isBefore(firstDayOfTheWeek.plusDays(7));

    }

    public static DateTime getWeekFromWeekNo(int weekOffset) {
        return minDate.plusWeeks(weekOffset);
    }


    /**
     * @return Returns the number of weeks between the current <code>date</code>
     * and the <code>minDate</code>.
     */
    public static int getWeeksSinceMinDate(Date date) {
        DateTime endTime = new DateTime(date);
        Interval interval = new Interval(minDate, endTime);
        return Weeks.weeksIn(interval).getWeeks();
    }
}
