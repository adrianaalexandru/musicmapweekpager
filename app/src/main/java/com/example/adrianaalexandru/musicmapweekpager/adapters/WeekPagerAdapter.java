package com.example.adrianaalexandru.musicmapweekpager.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.example.adrianaalexandru.musicmapweekpager.activities.fragments.WeekDaysFragment;
import com.example.adrianaalexandru.musicmapweekpager.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Adriana on 03/12/2016.
 */
public class WeekPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "WeekPagerAdapter";
    private ViewListener viewListener;
    private int totalWeekCount;
    private HashMap<Integer, WeekDaysFragment> pageReferenceMap;


    public WeekPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        totalWeekCount = DateUtils.getWeeksSinceMinDate(DateUtils.maxDate.toDate());
        pageReferenceMap = new HashMap<>();
    }

    public WeekDaysFragment getFragment(int key) {
        return pageReferenceMap.get(key);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        pageReferenceMap.remove(position);
    }

    @Override
    public Fragment getItem(int position) {
        WeekDaysFragment weekDaysFragment = WeekDaysFragment.newInstance(position, viewListener);
        pageReferenceMap.put(position, weekDaysFragment);
        return weekDaysFragment;
    }

    @Override
    public int getCount() {
        return totalWeekCount;
    }

    public ViewListener getViewListener() {
        return viewListener;
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public interface ViewListener extends Serializable {
        void onNewDaySelected(Date newSelectedDay);
    }


}
