package com.example.adrianaalexandru.musicmapweekpager.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Created by Adriana on 04/12/2016.
 */
public class ChangeViewWeightAnimation extends Animation {

    private float startWeight;
    private float deltaWeight;
    private View view;


    public ChangeViewWeightAnimation(View view, float startWeight, float endWeight) {
        this.startWeight = startWeight;
        this.deltaWeight = endWeight - startWeight;
        this.view = view;
        setFillAfter(true);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        layoutParams.weight = (startWeight + (deltaWeight * interpolatedTime));
        view.setLayoutParams(layoutParams);
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth,
                           int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}